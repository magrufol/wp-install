<?php


include_once('git/GitRepository.php');
include_once('helpers/functions.php');


$packageDataFile = file_get_contents("acc.json");
$packageData = json_decode($packageDataFile, true);

//var_dump($packageData);


$dbName = $packageData['dbname'];
$dbUser = $packageData['dbuser'];;
$dbPassword = $packageData['dbpass'];
$tablePreffix = substr($packageData['username'], 0, 2);
$siteName = $packageData['siteName'];
$sitePassword = $packageData['wpPass'];
$siteUser = $packageData['wpUser'];
$urlFull = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$url = str_replace('wp-install/setup/', '', $urlFull);
$path = str_replace('/wp-install/setup', '', parse_url($urlFull)['path']);
$adminMail = 'maxf@leos.co.il';
$install = false;

if(isset($_POST['install'])){
    $install = $_POST['install'];
}

if ($install) {

    $repo = GitRepository::cloneRepository('https://magrufol@bitbucket.org/leos-co-il/skeleton.git');
//$repo = true;

    $source = 'skeleton';
    $destination = str_replace('wp-install/setup', '', dirname(__FILE__));
    $renameResult = false;
    $saltVars = array(
        '[-AUTH_KEY-]',
        '[-SECURE_AUTH_KEY-]',
        '[-LOGGED_IN_KEY-]',
        '[-NONCE_KEY-]',
        '[-AUTH_SALT-]',
        '[-SECURE_AUTH_SALT-]',
        '[-LOGGED_IN_SALT-]',
        '[-NONCE_SALT-]');


    if ($repo) {
        if (file_exists($destination)) {
            if (is_dir($destination)) {
                if (is_writable($destination)) {
                    if ($handle = opendir($source)) {
                        while (false !== ($file = readdir($handle))) {
                            if(($file !== '..') && ($file !== '.git') && ($file !== '.')){
                                $renameResult = rename($source . '/' . $file, $destination . '/' . $file);
                            }
                        }
                        closedir($handle);
                    } else {
                        echo "$source could not be opened";
                    }
                } else {
                    echo "$destination is not writable!";
                }
            } else {
                echo "$destination is not a directory!";
            }
        } else {
            echo "$destination does not exist";
        }
    }

    if($renameResult){
        $wpConfigFile = $destination . '/wp-config.php';
        $htaccess = $destination . '/.htaccess';


        file_put_contents($wpConfigFile, str_replace('[-DB-NAME-]', $dbName, file_get_contents($wpConfigFile)));
        file_put_contents($wpConfigFile, str_replace('[-DB-USER-]', $dbUser, file_get_contents($wpConfigFile)));
        file_put_contents($wpConfigFile, str_replace('[-DB-PASSWORD-]', $dbPassword, file_get_contents($wpConfigFile)));
        file_put_contents($wpConfigFile, str_replace('[-PREFFIX-]', $tablePreffix, file_get_contents($wpConfigFile)));

        file_put_contents($htaccess, str_replace('[-SUB-URL-]', $path, file_get_contents($htaccess)));

        foreach ($saltVars as $salt){
            file_put_contents($wpConfigFile, str_replace($salt, getSalt(), file_get_contents($wpConfigFile)));
        }

    }


    $file = dirname(__FILE__) . '/skeleton.sql';
    file_put_contents($file, str_replace('[--SITE_URL--]', $url, file_get_contents($file)));
    file_put_contents($file, str_replace('[--SITE_HOME--]', $url, file_get_contents($file)));
    file_put_contents($file, str_replace('[--SITE_NAME--]', $siteName, file_get_contents($file)));
    file_put_contents($file, str_replace('[-PREFFIX-]', $tablePreffix, file_get_contents($file)));
    file_put_contents($file, str_replace('[--ADMIN_MAIL--]', $adminMail, file_get_contents($file)));
    file_put_contents($file, str_replace('[--ADMIN_USERNAME--]', $siteUser, file_get_contents($file)));
    file_put_contents($file, str_replace('[--MD_PSW--]', md5($sitePassword), file_get_contents($file)));

    //DB Import
    $filename = 'skeleton.sql';
    $mysqlHost = 'localhost';
    $connection = mysqli_connect($mysqlHost, $dbUser, $dbPassword, $dbName);


    if (mysqli_connect_errno())
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    $templine = '';
    $lines = file($filename);
    foreach ($lines as $line) {
        if (substr($line, 0, 2) == '--' || $line == '')
            continue;
        $templine .= $line;
        if (substr(trim($line), -1, 1) == ';') {
            if (!mysqli_query($connection, $templine)) {
                print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
            }
            $templine = '';
        }
    }
    mysqli_close($connection);
    echo '<div class="alert alert-success" role="alert">Database imported successfully</div>';


}

?>


    <!doctype html>
    <html lang="he" class="h-100">
    <head>
        <title>WP Install</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
              integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
              crossorigin="anonymous">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="assets/app.css">
    </head>
    <body class="h-100">


    <div class="container pt-5">
        <div class="row d-flex justify-content-center ">

            <div class="card db-settings" style="width: 40rem;">
                <div class="card-body">

                    <form id="settings" method="POST">
                        <div class="row d-flex align-items-center justify-content-center">
                            <div class="col-auto">
                                <div class="form-group ">
                                    <div class="input-group">
                                        <input type="hidden" class="form-control" id="install" name="install"
                                               value="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center justify-content-center">
                            <button type="submit" class="btn btn-primary">Start WordPress</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
    </body>
    </html>


<?php


?>